
**L’objectif étant de procéder à l’installation d’un environnement de travail permettant de réaliser différents travaux :**


Pour pouvoir mettre en place cette environnement de travail j’ai décidé de procédé à l’installation d’un dualboot de mon ordinateur permettant de séparer sont disque dure afin de pouvoir y héberger Windows d’une part et de l’autre Ubuntu pour mon cas.
**Pour cela j’ai suivi la vidéo suivante :** https://www.youtube.com/watch?v=coEXi2SInZQ
Une fois l’installation effectué j’ai décidé de procédé à cette installation d’un script structuré de cette manière :

**Dans un premier temps j’ai procédé à la mise à jour des paquets :**

	sudo apt update
	sudo apt upgrade


**Ensuite le script installe python et sont IDE vscode à l’aide des commande admin « sudo apt install » ainsi que les extensions nécessaires à vs code nous simplifiant le codage :**

	sudo apt install python3
	sudo apt install code
	code –install-extension ms-python.python
	code –install-extension njpwerner.autodocstring
	code –install-extension ritwickdey.liveserver
	code –install-extension fnando.linter
	code –install-extension vscjava.vscode-java-pack

**Après ça il se charge d’installer java à l’aide de la commande sudo de nouveaux :**


	sudo apt install default-jre
	sudo apt install default-jdk

**Pour finir le script se charge d’installer le Docker et de le lancer :**

	sudo apt install docker.io
	sudo usermod -aG docker $USER
	sudo systemctl start docker

**La suite du script permet de tester chaqu’une des installations que ce soit python, le docker ainsi que java il permet aussi de gérer les erreurs en les répertoriant dans un fichier erreur.txt il renvoie aussi si tout est opérationnel ou non.**

Pour effectuer cette instalation il suffit de récuperer le script et de l'éxecuter à l'aide de la commande ./<Nom du script> dans le terminale mais il faut aussi se mettre au préalable dans le fichier ou se situe se script.


